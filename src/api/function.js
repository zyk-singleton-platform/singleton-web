import request from '@/utils/request'

export function list() {
  return request({
    url: '/system/function/list',
    method: 'get'
  })
}

export function listByMenuId(params) {
  return request({
    url: '/system/function/listByMenuId',
    method: 'get',
    params
  })
}

export function newsFunction(data) {
  return request({
    url: '/system/function/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/system/function/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/system/function/deleted',
    method: 'post',
    data
  })
}
