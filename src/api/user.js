import request from '@/utils/request'

export function newUser(data) {
  return request({
    url: '/system/user/new',
    method: 'post',
    data
  })
}

export function pageFilter(data) {
  return request({
    url: '/system/user/pageFilter',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/system/user/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/system/user/deleted',
    method: 'post',
    data
  })
}
