import request from '@/utils/request'

export function newRole(data) {
  return request({
    url: '/system/role/new',
    method: 'post',
    data
  })
}

export function pageFilter(data) {
  return request({
    url: '/system/role/pageFilter',
    method: 'post',
    data
  })
}

export function list(data) {
  return request({
    url: '/system/role/list',
    method: 'get',
    data
  })
}

export function edited(data) {
  return request({
    url: '/system/role/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/system/role/deleted',
    method: 'post',
    data
  })
}
