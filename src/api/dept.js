import request from '@/utils/request'

export function list() {
  return request({
    url: '/system/dept/list',
    method: 'get'
  })
}

export function newsDept(data) {
  return request({
    url: '/system/dept/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/system/dept/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/system/dept/deleted',
    method: 'post',
    data
  })
}
