import request from '@/utils/request'
import qs from 'qs'

export function login(data) {
  return request({
    url: '/token/login',
    method: 'post',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    data: qs.stringify(data)
  })
}

export function getInfo() {
  return request({
    url: '/token/infos',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/token/logout',
    method: 'get'
  })
}
