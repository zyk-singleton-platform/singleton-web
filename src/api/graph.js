import request from '@/utils/request'
export function graph() {
  return request({
    url: '/captcha/graph',
    method: 'get'
  })
}
