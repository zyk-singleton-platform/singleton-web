import request from '@/utils/request'

export function tree() {
  return request({
    url: '/system/api/tree',
    method: 'get'
  })
}
