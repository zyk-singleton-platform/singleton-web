import request from '@/utils/request'

export function list() {
  return request({
    url: '/system/platform/list',
    method: 'get'
  })
}
