import request from '@/utils/request'

export function list() {
  return request({
    url: '/system/menu/list',
    method: 'get'
  })
}

export function currentMenu(params) {
  return request({
    url: '/system/menu/current',
    method: 'get',
    params
  })
}

export function tree(params) {
  return request({
    url: '/system/menu/tree',
    method: 'get',
    params
  })
}

export function sync(data, params) {
  return request({
    url: '/system/menu/sync',
    method: 'post',
    data,
    params
  })
}

export function newsMenu(data) {
  return request({
    url: '/system/menu/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/system/menu/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/system/menu/deleted',
    method: 'post',
    data
  })
}
