const table = {
  data() {
    return {
      tableHeight: '400',
      showTable: false
    }
  },
  mounted() {
    const tableView = this.$refs['table-view']
    if (tableView) {
      this.$nextTick(() => {
        this.tableHeight = tableView.clientHeight
        this.showTable = true
      })
    }
  }
}

export default table
